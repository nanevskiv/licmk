import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import Login from './user/login/Login';
import Signup from './user/signup/Signup';
import Profile from './user/profile/Profile';
import OAuth2RedirectHandler from './user/oauth2/OAuth2RedirectHandler';
import LoadingIndicator from './common/LoadingIndicator';
import {getCurrentUser} from './util/APIUtils';
import {ACCESS_TOKEN} from './constants';
import PrivateRoute from './common/PrivateRoute';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import './App.css';
import ProductList from "./components/ProductList";
import Navbar from "./components/Navbar";
import Details from "./components/Details";
import Cart from "./components/Cart";
import Chat from "./components/Chat";
import NotFound from "./common/NotFound";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: false,
            currentUser: null,
            loading: false
        };

        this.loadCurrentlyLoggedInUser = this.loadCurrentlyLoggedInUser.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    loadCurrentlyLoggedInUser() {
        this.setState({
            loading: true
        });

        getCurrentUser()
            .then(response => {
                this.setState({
                    currentUser: response,
                    authenticated: true,
                    loading: false
                });
            }).catch(error => {
            this.setState({
                loading: false
            });
        });
    }

    handleLogout() {
        localStorage.removeItem(ACCESS_TOKEN);
        this.setState({
            authenticated: false,
            currentUser: null
        });
        Alert.success("You're safely logged out!");
    }

    componentDidMount() {
        this.loadCurrentlyLoggedInUser();
    }

    render() {
        if (this.state.loading) {
            return <LoadingIndicator/>
        }

        return(
            <React.Fragment>
                <Navbar/>
                <Switch>
                    <Route exact path="/" component={ProductList}/>
                    <Route path="/details" component={Details}/>
                    <Route path="/login"
                           render={(props) => <Login
                               authenticated={this.state.authenticated} {...props} />}/>
                    <Route path="/cart" component={Cart}/>
                    <PrivateRoute path="/profile" authenticated={this.state.authenticated}
                                  currentUser={this.state.currentUser}
                                  component={Profile}/>
                    <Route path="/signup"
                           render={(props) => <Signup
                               authenticated={this.state.authenticated} {...props} />}/>
                    <Route path="/oauth2/redirect" component={OAuth2RedirectHandler}/>
                    <PrivateRoute path="/chat" authenticated={this.state.authenticated}
                                  component={Chat}/>
                    <Route component={NotFound}/>
                </Switch>
                <Alert stack={{limit: 3}}
                       timeout={3000}
                       position='top-right' effect='slide' offset={65}/>
            </React.Fragment>
        );
    }
}

export default App;
