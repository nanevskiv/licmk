import React, { Component } from 'react';
import './NotFound.css';
import { Link } from 'react-router-dom';
import {BackButton} from "../components/BackButton";

class NotFound extends Component {
    render() {
        return (
            <div className="page-not-found">
                <h1 className="title">
                    404
                </h1>
                <div className="desc">
                    The Page you're looking for was not found.
                </div>
                <Link to="/">
                    <BackButton>
                        Назад кон продукти
                    </BackButton>
                </Link>
            </div>
        );
    }
}

export default NotFound;