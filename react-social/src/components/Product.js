import React, { Component } from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'
import {ProductConsumer} from "../context";

class Product extends Component{
    render(){
        const {id, title, img, price, startDate, startTime} = this.props.product;
        return(
            <ProductWrapper className="col-9 col-md-6 col-lg-3 my-3">
                <div className="card">
                    <ProductConsumer>
                        {(value) => (<div className="img-container p-5" onClick={ () => value.handleDetail(id)}>
                            <Link to="/details">
                                <img src={img} alt="product_img" className="card-img-top"/>
                            </Link>
                        </div>)}
                    </ProductConsumer>
                    <div className="card-footer d-flex justify-content-between">
                        <p className="align-self-center mb-0">{title}</p>
                        <h5 className="mb-2">
                            {price}
                            <span>ден</span>
                        </h5>
                    </div>
                    <div className="card-footer d-flex justify-content-between">
                        <p className="align-self-center mb-0">Почеток на лицитација на:</p>
                    </div>
                    <div className="card-footer d-flex justify-content-between">
                        <strong><span>Датум: </span>{startDate}</strong>
                        <strong><span>Време: </span>{startTime}</strong>
                    </div>
                </div>
            </ProductWrapper>
        )
    }
}

const ProductWrapper = styled.div`
  .card{
    border-color: transparent;
    transition: all 0.5s linear;
  }
  .card-footer{
    background: transparent;
    border-top: transparent;
    transition: all 0.5 linear;
  }
  &:hover{
    .card{
      border: 0.05rem solid #C0C0C0;
    }
  }
  .img-container{
    position: relative;
    overflow: hidden;
  }
  .card-img-top{
    transition: all 0.4s linear;
  }
  .img-container:hover .card-img-top{
    transform: scale(1.2);
  }
`;

export default Product;

