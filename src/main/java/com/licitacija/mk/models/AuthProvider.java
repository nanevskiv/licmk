package com.licitacija.mk.models;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
